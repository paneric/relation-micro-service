window.onload = function () {
    hookModalTriggers();
    hookModalCloseBtn();
};

function hookModalTriggers(){
    let modalTriggers = document.getElementsByClassName('modal-trigger');
    let modal = document.getElementById('app-modal');

    for (let i = 0; i < modalTriggers.length; i++) {

        modalTriggers[i].addEventListener('click', function () {
            modal.style.display = 'block';
        });
    }
}

function hookModalCloseBtn(){
    let modalCloseBtn = document.getElementById('app-modal-close');
    let modal = document.getElementById('app-modal');

    modalCloseBtn.addEventListener('click', function (e) {
        modal.style.display = 'none';
    });
}

window.onclick = function(event) {
    let modal = document.getElementById('app-modal');

    if (event.target === modal) {
        modal.style.display = 'none';
    }
}
