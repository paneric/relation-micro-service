-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Czas generowania: 06 Sty 2021, 15:38
-- Wersja serwera: 5.7.32-0ubuntu0.18.04.1
-- Wersja PHP: 7.3.25-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `e_commerce`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sessions`
--

CREATE TABLE `sessions` (
  `ses_id` int(11) UNSIGNED NOT NULL,
  `ses_session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ses_ip_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ses_user_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ses_data` text COLLATE utf8_unicode_ci NOT NULL,
  `ses_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ses_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`ses_id`),
  ADD UNIQUE KEY `ses_session_id` (`ses_session_id`),
  ADD KEY `ses_created_at` (`ses_created_at`),
  ADD KEY `ses_ip_address` (`ses_ip_address`),
  ADD KEY `ses_updated_at` (`ses_updated_at`),
  ADD KEY `ses_user_agent` (`ses_user_agent`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `sessions`
--
ALTER TABLE `sessions`
  MODIFY `ses_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
